import React from 'react';
import TodoList from './Todo/TodoList';
import Context from "./context";
import AddTodo from './Todo/AddTodo'


function App() {
    const [todos, setTodos] = React.useState([{
        id: 0, status: "In progress", topic: "Create Todo List",
        description: "react", tag: "W", dateCreate: "09.02.2021", deadline: "28.02.2021"
    },
        {
            id: 1, status: "Done", topic: "Create Todo List",
            description: "react2", tag: "W", dateCreate: "09.02.2021", deadline: "28.02.2021"
        },
        {
            id: 2, status: "Todo", topic: "Create Todo List",
            description: "react3", tag: "W", dateCreate: "09.02.2021", deadline: "28.02.2021"
        },
    ])

    function removeTodo(id) {
        setTodos(todos.filter(todo => todo.id !== id))
    }

    function addTodo(topic) {
        setTodos(
            todos.concat([
                {
                    topic,
                    id: Date.now()
                }
            ])
        )
    }

    return (
        <Context.Provider value={{removeTodo}}>
            <div className="wrapper">
                <h1>Todo List</h1>
                <AddTodo onCreate={addTodo}></AddTodo>
                {todos.length ? (
                    <TodoList todos={todos}/>
                ) : <p>No todos!</p>
                }
            </div>
        </Context.Provider>
    );
}

export default App
