import React, { useContext, useState } from 'react'
import PropTypes from 'prop-types'
import Context from '../context'


const styles ={
    li:{
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: '.5rem 1rem',
      border: '1px solid #ccc',
      borderRadius: '4px',
      marginBottom: '.5rem'
    }
  }

function TodoDescription({isClick, description}){
    return isClick ? (<div>{description}</div>) : null;
}

function TodoItem({todo, index}){
    const { removeTodo } = useContext(Context)
    const [isClicked, setIsClicked] = useState(false)
    return(
        <li>
            <div style = {styles.li}>
                <span>
                    <strong> {index + 1}</strong>
                    &nbsp;
                    {todo.tag}
                    &nbsp;
                    &nbsp;
                    <button onClick = {() => setIsClicked(!isClicked)}>{todo.topic}</button>
                    &nbsp;&nbsp;
                    {todo.dateCreate}
                    &nbsp;&nbsp;
                    {todo.deadline}
                </span>
                <button className ='remove' onClick={removeTodo.bind(null, todo.id)}>
                    &times;
                </button>
            </div>
            <TodoDescription description = {todo.description} isClick = {isClicked}/>
        </li>
    )
}

TodoItem.propTypes = {
    todo: PropTypes.object.isRequired,
    index: PropTypes.number,
    isClick: PropTypes.bool
}

export default TodoItem